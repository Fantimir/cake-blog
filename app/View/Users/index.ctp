<h1>Users</h1>
<?php echo $this->Html->link('Add User',array('controller' => 'users', 'action' => 'add')); ?>
<table>
    <tr>
        <th>Id</th>
        <th>Username</th>
        <th>Role</th>
    </tr>

   
    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['id']; ?></td>
        <td><?php echo $user['User']['username']?></td>
        <td><?php echo $user['User']['role']?></td>
        
    </tr>
    <?php endforeach; ?>

</table>